import java.io.Console;
import java.util.Arrays;
import java.util.Comparator;

public class Sheep {

    enum Animal {sheep, goat}

    public static void main(String[] param) {
        // for debugging

    }

    public static void reorder(Animal[] animals) {
        // TODO!!! Your program here
        Animal[] sorted = new Animal[animals.length];
        int left = 0;
        int right = animals.length - 1;
        for (Animal animal : animals) {
            if (animal == Animal.goat) {
                sorted[left] = animal;
                left++;
            } else {
                sorted[right] = animal;
                right--;
            }
        }
         System.arraycopy(sorted, 0, animals, 0, sorted.length);
    }

        //sort(animals, 0, animals.length -1);
    } // sort() lopp

   /* public static void binarySort(Animal[] animals){
        if (animals.length < 2) return;
        int pos; //postion of the key element
        Animal key;
        //insertion sort
        for(int i=1;i<animals.length;i++){
            key=animals[i];
            pos=binarySearch(0,i-1,key,animals); //finds where the element will be stored
            for(int j=i;j>pos;j--) //shifts the other elements by 1 position to the right
                animals[j]=animals[j-1];
            animals[pos]=key; //places the key element in the pos position
        }

    }
    //uses binary search technique to find the position where the element will be inserted
    public static int binarySearch(int low, int high, Animal key, Animal[] animals){
        int mid;
        while(low<=high){
            mid=(low+high)/2;
            if(comparator.compare(key, animals[mid]) >0)
                low=mid+1;
            else if(comparator.compare(key,animals[mid]) <0)
                high=mid-1;
            else
                return mid;
        }
        return low;
    }















    private static void sort(Animal[] animals, int l, int r) {
        if (animals.length < 2) return;
        int i = l;
        int j = r;
        Animal x = animals[(i + j) / 2];
        do {
            while (animals[i]==Animal.sheep && animals[i] == x) i++;
            while (x==Animal.sheep && x == animals[j]) j--;
            if (i <= j) {
                Animal tmp = animals[i];
                animals[i] = animals[j];
                animals[j] = tmp;
                // selle koha peal on väljatrükk silumiseks: "veelahe" ja vahetatavad
                i++;
                j--;
            }
        } while (i < j);
        if (l < j) sort(animals, l, j); // rekursioon
        if (i < r - 1) sort(animals, i, r); // rekursioon
    }

    protected static Comparator<Animal> comparator = (o1,o2) -> {
        if (o1 == Animal.goat && o2 == Animal.sheep)
            return -1;

        else if (o1 == Animal.sheep && o2 == Animal.goat)
            return 1;
        return 0;
    };


    static int partition(Animal[] arr, int left, int right)
    {
        int i = left, j = right;
        Animal tmp;
        Animal pivot = arr[(left + right) / 2];

        while (i <= j) {
            while (comparator.compare(arr[i], pivot) < 0)
                i++;
            while (comparator.compare(pivot, arr[j]) < 0)
                j--;
            if (i <= j) {
                tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
                i++;
                j--;
            }
        };

        return i;
    }

    static void quickSort(Animal[] arr, int left, int right) {
        if (arr.length < 2) return;
        int index = partition(arr, left, right);
        if (left < index - 1)
            quickSort(arr, left, index - 1);
        if (index < right)
            quickSort(arr, index, right);
    }
}
*/




